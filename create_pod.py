import os
from kubernetes import client, config
config.load_kube_config()

SDP_NAMESPACE = os.environ.get("KUBE_NAMESPACE_SDP", "ska-tmc-integration-sdp")

POD_CONTAINER = "data-prep"
POD_COMMAND = [
    "/bin/bash",
    "-c",
    "apt-get update && apt-get install -y git git-lfs;"
    "git clone -n --depth=1 --filter=tree:0 --branch=0.20.0 "
    "https://gitlab.com/ska-telescope/sdp/ska-sdp-integration.git; cd ska-sdp-integration;"
    "git checkout; git lfs fetch; cd tests/resources/data/pointing-data;"
    "mkdir -p {path}; cp -r *.ms {path};"
    " trap : TERM; sleep infinity & wait",
]

DATA_POD_DEF = {
    "apiVersion": "v1",
    "kind": "Pod",
    "metadata": {"name": "receive-data"},
    "spec": {
        "securityContext": {"runAsUser": 0},  # run as root so that we can download data
        "containers": [
            {
                "image": "artefact.skao.int/ska-sdp-realtime-receive-modules:5.0.0",  # noqa: E501
                "name": POD_CONTAINER,
                "command": POD_COMMAND,
                "volumeMounts": [{"mountPath": "/mnt/data", "name": "data"}],
            }
        ],
        "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "testing"}}],
    },
}

core_api = client.CoreV1Api()
pod_spec = DATA_POD_DEF.copy()

# Update the name of the pod and the data PVC
pod_spec["metadata"]["name"] = "sdp-test-data"
pod_spec["spec"]["volumes"][0]["persistentVolumeClaim"]["claimName"] = "test-pvc"
pod_spec["spec"]["containers"][0]["command"][2] = (
    pod_spec["spec"]["containers"][0]["command"][2].format(path="/mnt/data/product/eb-test-20210630-00001/ska-sdp/pb-test-20211111-00001/"))

# Check Pod does not already exist
k8s_pods = core_api.list_namespaced_pod(SDP_NAMESPACE)
for item in k8s_pods.items:
    assert (
        item.metadata.name != pod_spec["metadata"]["name"]
    ), f"Pod {item.metadata.name} already exists"

core_api.create_namespaced_pod(SDP_NAMESPACE, pod_spec)

