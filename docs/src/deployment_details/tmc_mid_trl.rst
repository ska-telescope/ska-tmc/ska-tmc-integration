######################################################
TRL's for TMC Mid devices
######################################################


The TRL's of all the TMC Mid devices are updated to comply with ADR-9

+-------------------------+----------------------------------------+---------------------------------------+
|  **TMC Mid Node**       |        **Previous TRL**                | **Updated TRL**                       |
+-------------------------+----------------------------------------+---------------------------------------+
| Central Node            | `ska_mid/tm_central/central_node`      | `mid-tmc/central-node/0`              |
+-------------------------+----------------------------------------+---------------------------------------+
| Subarray Node           |`ska_mid/tm_subarray_node/1`            | `mid-tmc/subarray/01`                 |
+-------------------------+----------------------------------------+---------------------------------------+
| CSP Master              |`ska_mid/tm_leaf_node/csp_master`       | `mid-tmc/leaf-node-csp/0`             |
+-------------------------+----------------------------------------+---------------------------------------+
| SDP Master              |`ska_mid/tm_leaf_node/sdp_master`       | `mid-tmc/leaf-node-sdp/0`             |
+-------------------------+----------------------------------------+---------------------------------------+
| Dish Leaf Node          |`ska_mid/tm_leaf_node/d0001`            | `mid-tmc/leaf-node-dish/SKA001`       |
+-------------------------+----------------------------------------+---------------------------------------+
| CSP Subarray Leaf Node  |`ska_mid/tm_leaf_node/csp_subarray01`   | `mid-tmc/subarray-leaf-node-csp/01`   |
+-------------------------+----------------------------------------+---------------------------------------+
| SDP Subarray Leaf Node  |`ska_mid/tm_leaf_node/sdp_subarray01`   | `mid-tmc/subarray-leaf-node-sdp/01`   |
+-------------------------+----------------+-----------------------+---------------------------------------+