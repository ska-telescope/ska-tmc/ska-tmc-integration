name: "centralnode-test"
function: telescope-monitoring
domain: general-monitoring
instances: ["01"]
{{- $DishMasterIdentifier := .Values.global.dish_suffix }}
{{- $KValueValidRangeUpperLimit := .Values.deviceServers.centralnode.KValueValidRange.max | int }}
{{- $KValueValidRangeLowerLimit := .Values.deviceServers.centralnode.KValueValidRange.min | int }}

{{- if ge $KValueValidRangeUpperLimit $KValueValidRangeLowerLimit }}
entrypoints:
  - name: "ska_tmc_centralnode.central_node_mid.MidTmcCentralNode"
server:
  name: "central_node_mid"
  instances:
  {{- $tmc_sa_fqdn_prefix:= .Values.global.tmc_subarray_prefix}}
  {{- $csp_sa_ln_fqdn_prefix:= .Values.global.csp_subarray_ln_prefix}}
  {{- $sdp_sa_ln_fqdn_prefix:= .Values.global.sdp_subarray_ln_prefix}}
  {{- $isSimulatedDish := .Values.deviceServers.mocks.dish }}
  {{- $real_dish_name := .Values.global.namespace_dish.dish_names }}
  {{- $isSimulatedCsp := .Values.deviceServers.mocks.csp }}
  {{- $mocks_enabled := .Values.deviceServers.mocks.enabled}}
  {{- $CommandTimeOut := .Values.deviceServers.centralnode.CommandTimeOut}}
  {{- $DishVccInitTimeout := .Values.deviceServers.centralnode.DishVccInitTimeout}}
  {{- $LivelinessCheckPeriod := .Values.deviceServers.centralnode.LivelinessCheckPeriod}}
  {{- $EventSubscriptionCheckPeriod := .Values.deviceServers.centralnode.EventSubscriptionCheckPeriod}}
  {{- $domain := .Values.global.domain }}
  {{- $family := .Values.deviceServers.centralnode.family }}
  {{- $member := .Values.deviceServers.centralnode.member }}
  - name: "01"
    classes:
    - name: "MidTmcCentralNode"
      devices:
      - name: "{{ $domain }}/{{ $family }}/{{ $member }}"
        properties:
        - name: "CspMasterLeafNodeFQDN"
          values:
          - "{{.Values.global.csp_master_ln}}"
        - name: "CspMasterFQDN"
          values:
          - "{{.Values.global.csp_master}}"
        - name: "SdpMasterLeafNodeFQDN"
          values:
          - "{{.Values.global.sdp_master_ln}}"
        - name: "SdpMasterFQDN"
          values:
          - "{{.Values.global.sdp_master}}"
        - name: "DishLeafNodePrefix"
          values:
          - "{{lower .Values.global.dish_ln_prefix}}"
        - name: "DishMasterIdentifier"
          values:
          - "{{ $DishMasterIdentifier }}"
        - name: "DishVccUri"
          values:
          - "{{.Values.deviceServers.centralnode.DishVccConfig.DishVccUri}}"
        - name: "DishVccFilePath"
          values:
          - "{{.Values.deviceServers.centralnode.DishVccConfig.DishVccFilePath}}"
        - name: "EnableDishVccInit"
          values:
          - "{{.Values.deviceServers.centralnode.DishVccConfig.Enabled}}"
        - name: "DishKvalueAggregationAllowedPercent"
          values:
          - "{{.Values.deviceServers.centralnode.DishVccConfig.DishKvalueAggregationAllowedPercent}}"
        - name: "DishIDs"
          values:
          {{- range  (.Values.deviceServers.centralnode.DishIDs)  }}
          - "{{ . }}"
          {{- end }}
        - name: "DishMasterFQDNs"
          values:
          {{- if and (eq $mocks_enabled true) (eq $isSimulatedDish true) }}
          {{- range  (.Values.deviceServers.centralnode.DishIDs)  }}
          - "{{ $DishMasterIdentifier }}/{{ . | lower }}"
          {{- end }}
          {{- else }}
          {{- range  (.Values.global.namespace_dish.dish_names)  }}
          - "{{ . }}" # Real dish
          {{- end }}
          {{- end }}
        - name: "TMCSubarrayNodes"
          values:
          {{- range  ( regexSplit " " ((.Values.subarray_count | int) | seq ) -1 )  }}
          - "{{ $tmc_sa_fqdn_prefix }}/{{ printf "%02s" . }}"
          {{- end }}
        - name: "CspSubarrayLeafNodes"
          values:
          {{- range  ( regexSplit " " ((.Values.subarray_count | int) | seq ) -1 )  }}
          - "{{ $csp_sa_ln_fqdn_prefix }}{{ printf "%02s" . }}"
          {{- end }}
        - name: "SdpSubarrayLeafNodes"
          values:
          {{- range  ( regexSplit " " ((.Values.subarray_count | int) | seq ) -1 )  }}
          - "{{ $sdp_sa_ln_fqdn_prefix }}{{ printf "%02s" . }}"
          {{- end }}
        - name: "KValueValidRangeUpperLimit"
          values:
          - "{{ $KValueValidRangeUpperLimit }}"
        - name: "KValueValidRangeLowerLimit"
          values:
          - "{{ $KValueValidRangeLowerLimit }}"
        - name: "SkaLevel"
          values:
            - "1"
        - name: "LoggingTargetsDefault"
          values:
            - "tango::logger"
        - name: "LoggingLevelDefault"
          values:
            - "5"
        - name: "DishVccInitTimeout"
          values:
            - "{{ $DishVccInitTimeout }}"
        - name: "CommandTimeOut"
          values:
            - "{{ $CommandTimeOut }}"
        - name: "LivelinessCheckPeriod"
          values:
            - "{{ $LivelinessCheckPeriod }}"
        - name: "EventSubscriptionCheckPeriod"
          values:
            - "{{ $EventSubscriptionCheckPeriod }}"

{{- else }}
{{- fail "KValueValidRangeUpperLimit should be greater than KValueValidRangeLowerLimit" }}
{{- end }}

depends_on:
  - device: sys/database/2
image:
  registry: "{{.Values.centralnode.image.registry}}"
  image: "{{.Values.centralnode.image.image}}"
  tag: "{{.Values.centralnode.image.tag}}"
  pullPolicy: "{{.Values.centralnode.image.pullPolicy}}"

